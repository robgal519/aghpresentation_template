SHELL:=/bin/bash -O extglob
NAME = prezentacja

ALL=$(wildcard *.sty *.tex *.png *.svg *.jpg)

all: $(NAME).pdf

recompile: clean
	make all

$(NAME).pdf: *.tex *.bib 
	pdflatex -file-line-error --max-print-line=200 -synctex=1  -shell-escape $(NAME) && bibtex $(NAME) && pdflatex -shell-escape $(NAME) && pdflatex -shell-escape $(NAME)

show: $(NAME).pdf
	xdg-open $<

clean:
	rm -rf !(*.tex|*.sty|*.bib|examples|makefile|*.png)


watch:  ## Recompile on any update of LaTeX or image
	while true; do inotifywait $(ALL); sleep 1; make all; done;


